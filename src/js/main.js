window.$ = window.jQuery = require('jquery');
const owl_carousel = require('owl.carousel');
const AOS = require('aos');
const offcanvas = require('hc-offcanvas-nav');
const Swal = require('sweetalert2');
// const slick = require('slick-carousel');
// const select2 = require('select2');
// const axios = require('axios'); 
// const mask = require('jquery-mask-plugin');
window.onload = function() {
    let $main_nav = $('.main-nav');
    let $toggle = $('.toggle');

    let defaultData = {
        maxWidth: 576,
        customToggle: $toggle,
        levelTitles: false,
        navTitle: null,
        insertClose: false,
        labelBack: 'Назад',
    }
    let Nav = $main_nav.hcOffcanvasNav(defaultData);
    if (!window.matchMedia('(max-width: 576px)').matches) {
        AOS.init();
        var lastScrollTop = 0;
        
        $(window).scroll(function(event){
            var scrollPosition = window.scrollY;
            if (scrollPosition < 400 && $('.header').hasClass('banner--clone')) {
                $('.header').removeClass('banner--clone');
            }
            var showHeaderPosition = 400;
            var st = $(this).scrollTop();
            
            if (st > lastScrollTop && scrollPosition > showHeaderPosition) {
                $('.header').addClass('banner--clone');
                if ($('.header').hasClass('banner--unstick')) {
                    $('.header').removeClass('banner--unstick');
                }
                $('.header').addClass('banner--stick'); 
            } else if(st < lastScrollTop && scrollPosition > showHeaderPosition) {
                if ($('.header').hasClass('banner--stick')) {
                    $('.header').removeClass('banner--stick');
                }
                $('.header').addClass('banner--unstick');
            }
            lastScrollTop = st;
        });
    }
    else {
        document.querySelectorAll('[data-aos]').forEach(aos => {
            aos.removeAttribute('data-aos');
        })
    }
    this.document.querySelectorAll('.feedback-btn').forEach(btn => btn.addEventListener('click', function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
        Swal.fire({
            showCloseButton: true,
            title: 'Обратная связь',
            confirmButtonText: 'Отправить',
            html:
                '<p class="swal2-description">Заполните данные в форме ниже и наш менеджер свяжется с Вами в ближайшее время.</p>' +
                '<input id="initials" type="text" class="swal2-input" placeholder="ФИО">' +
                '<input id="phone-number" type="text" class="swal2-input" placeholder="Номер телефона">',
            preConfirm: () => {
                return ([
                    document.getElementById('initials').value,
                    document.getElementById('phone-number').value
                ])
            }            
        })
        .then((result) => {
            if (result.value) {
                Toast.fire({
                    icon: 'success',
                    title: 'Вы успешно подписались'
                });
            }
        })
    }));
    

    let vid = document.getElementsByTagName('video')[0];

    vid.addEventListener('click', function() {
        document.getElementsByClassName('show__video')[0].style.display = 'none';
        if (this.paused) {
            this.play();
        } else {
            this.pause();
        }
    });

    this.document.getElementsByClassName('show__video')[0].addEventListener('click', function() {
        this.style.display = 'none';
        if (vid.paused) {
            vid.play();
        } else {
            vid.pause();
        }
    });
    
    document.querySelectorAll('.main-nav a').forEach(a => a.addEventListener('click', function(e) {
        document.querySelector('.main-nav a.active').classList.remove('active');
        if (!a.classList.contains('active')) {
            a.classList.add('active');
        }
    }));

    let services = document.querySelectorAll('.description-col');
    let active = services[1];

    services.forEach(service => service.addEventListener('click', function(e) {
        if (active) {
            active.classList.remove('active-col');
        }
        e.currentTarget.classList.add('active-col');
        active = e.currentTarget;
    }));

    $('.reviews__carousel').owlCarousel({
        items: 2,
        slideBy: 2,
        margin: 10,
        loop: true,
        nav: true,
        dots: true,
        animateIn: true,
        animateOut: true,
        responsive:{
            0:{
                items: 1,
                nav: false,
                dots: false,
            },
            600:{
                nav: true,
                dots: true,
            },
            1000:{
                nav: true,
                dots: true,
            }
        }
    });

    let container = document.createElement('div')
    container.classList.add('container');
    let col = document.createElement('div');
    col.classList.add('col-lg-12', 'my-5', 'd-flex', 'justify-content-center', 'flex-nowrap', 'align-items-center');
    let row = document.createElement('div');
    row.classList.add('row');
    document.getElementsByClassName('owl-nav')[0].insertBefore(this.document.getElementsByClassName('owl-dots')[0], document.getElementsByClassName('owl-nav')[0].children[1]);
    col.appendChild(document.getElementsByClassName('owl-nav')[0]);
    
    row.appendChild(col);
    container.appendChild(row);
    this.document.getElementsByClassName('reviews__carousel')[0].appendChild(container);

    let canvas = document.getElementsByTagName("canvas")[0];
    let canvas2 = document.getElementsByTagName("canvas")[1];
    canvas2.setAttribute('width', document.documentElement.offsetWidth);
    canvas2.setAttribute('height', document.documentElement.scrollHeight);
    canvas.setAttribute('width', document.documentElement.offsetWidth);
    canvas.setAttribute('height', document.documentElement.scrollHeight);
    
    function drawLines() {
        let ctx = canvas.getContext("2d");
        for (let i = 4; i >= 0; i--) {
            ctx.lineWidth = 1;
            ctx.strokeStyle  = 'rgb(237, 237, 237)';
              
            ctx.beginPath();
            ctx.moveTo(document.documentElement.offsetWidth - (130+i*270),0);
            ctx.lineTo(document.documentElement.offsetWidth - (130+i*270),document.documentElement.scrollHeight);
            ctx.stroke();
            ctx.closePath();
        }
        for (let i = 0; i < 7; i++) {
            ctx.lineWidth = 1;
            ctx.strokeStyle  = 'rgb(237, 237, 237)';
            ctx.beginPath();
            ctx.moveTo(0,130+i*590);
            ctx.lineTo(document.documentElement.offsetWidth,130+i*590);
            ctx.stroke();
            ctx.closePath();
        }
        let min = 150;
        let max = document.documentElement.scrollHeight - (130+0*270);
        let random;
        for (let i = 4; i >= 0; i--) {
            for (let j = 15; j > 0; j--) {
                ctx.save();
                random = Math.ceil(min + Math.random() * (max - min));
                ctx.lineWidth = 3;
                ctx.strokeStyle = 'rgba(237, 237, 237, .6)';
                ctx.beginPath();
                ctx.moveTo(document.documentElement.offsetWidth - (136+j*270), random);
                ctx.lineTo(document.documentElement.offsetWidth - (136+j*270) + 12, random);
                random = 0;
                ctx.stroke();
                ctx.closePath();
                ctx.restore();
                ctx.save();
                random = Math.ceil(min + Math.random() * (max - min));
                ctx.lineWidth = 3;
                ctx.strokeStyle = 'rgba(237, 237, 237, .6)';
                ctx.beginPath();
                ctx.moveTo(document.documentElement.offsetWidth - (136+j*270), random);
                ctx.lineTo(document.documentElement.offsetWidth - (136+j*270) + 12, random);
                ctx.moveTo(document.documentElement.offsetWidth - (136+j*270) + 6, random + 7);
                ctx.lineTo(document.documentElement.offsetWidth - (136+j*270) + 6, random - 7);
                random = 0;
                ctx.stroke();
                ctx.closePath();
                ctx.restore();
            }
        }
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.translate(document.documentElement.offsetWidth - (135+4*270), 300);
        ctx.rotate(-Math.PI/2);
        ctx.fillText("navigator-sg", 0, 0);  
        ctx.fill();
        
        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.translate(document.documentElement.offsetWidth - (125+0*270), 400);
        ctx.rotate(Math.PI/2);
        ctx.fillText("security system", 0, 0);
        ctx.fill();
        
        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.translate(document.documentElement.offsetWidth - (125+2*266), 500);
        ctx.rotate(Math.PI/2);
        ctx.fillText("Листай дальше", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (135+4*270), 1000);
        ctx.rotate(-Math.PI/2);
        ctx.fillText("О компании", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (125+0*270), 1300);
        ctx.rotate(Math.PI/2);
        ctx.fillText("About company", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (135+4*270), 1750);
        ctx.rotate(-Math.PI/2);
        ctx.fillText("Преимущества", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (125+0*270), 2100);
        ctx.rotate(Math.PI/2);
        ctx.fillText("Advantages", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (135+4*270), 2300);
        ctx.rotate(-Math.PI/2);
        ctx.fillText("Наши услуги", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (125+0*270), 2600);
        ctx.rotate(Math.PI/2);
        ctx.fillText("Our services", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (135+4*270), 3200);
        ctx.rotate(-Math.PI/2);
        ctx.fillText("Отзывы клиентов", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (125+0*270), 3300);
        ctx.rotate(Math.PI/2);
        ctx.fillText("Customer reviews", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (135+4*270), 3700);
        ctx.rotate(-Math.PI/2);
        ctx.fillText("Контакты", 0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();
        ctx.beginPath();
        ctx.font = "14px AvenirNextCyr";
        ctx.fillStyle = "rgb(27, 28, 36)";
        ctx.translate(document.documentElement.offsetWidth - (125+0*270), 4000);
        ctx.rotate(Math.PI/2);
        ctx.fillText("Contacts", 0, 0);
        ctx.fill();
    }
    let ctx = canvas2.getContext('2d');
    let arrX = [];
    for (let i = 4; i >= 0; i--) {
        arrX.push(document.documentElement.offsetWidth - (136+i*270))
    }
    function randomXPosition(arr) {
        var rand = Math.floor(Math.random() * arr.length);
        return arr[rand];
    }
    let dotParametrs = {
        x: randomXPosition(arrX),
        y: document.documentElement.scrollHeight,
        speed: 3,
        delay: 1,
        src: '../images/dot.png',
        horizontalCount: function() {
            let horizontalLinesArr =  [];
            for (let i = 0; i < 7; i++) {
                horizontalLinesArr.push(123+i*590 + 1);
            }
            this['horizontalLines'] = horizontalLinesArr;
        },
        verticalCount: function() {
            let verticalLinesArr = [];
            for (let i = 4; i >= 0; i--) {
                verticalLinesArr.push(document.documentElement.offsetWidth - (136+i*270));
            }
            this['verticalLines'] = verticalLinesArr;
        }
    }
    dotParametrs.horizontalCount();
    dotParametrs.verticalCount();
    let dotParametrs2 = {
        x: randomXPosition(arrX),
        y: 0,
        speed: 3,
        delay: 1,
        src: '../images/dot.png',
        horizontalCount: function() {
            let horizontalLinesArr =  [];
            for (let i = 0; i < 7; i++) {
                horizontalLinesArr.push(123+i*590 + 1);
            }
            this['horizontalLines'] = horizontalLinesArr;
        },
        verticalCount: function() {
            let verticalLinesArr = [];
            for (let i = 4; i >= 0; i--) {
                verticalLinesArr.push(document.documentElement.offsetWidth - (136+i*270));
            }
            this['verticalLines'] = verticalLinesArr;
        }
    }
    dotParametrs2.horizontalCount();
    dotParametrs2.verticalCount();
    let dotParametrs3 = {
        x: randomXPosition(arrX),
        y: 0,
        speed: 3,
        delay: 1,
        src: '../images/dot.png',
        horizontalCount: function() {
            let horizontalLinesArr =  [];
            for (let i = 0; i < 7; i++) {
                horizontalLinesArr.push(123+i*590 + 1);
            }
            this['horizontalLines'] = horizontalLinesArr;
        },
        verticalCount: function() {
            let verticalLinesArr = [];
            for (let i = 4; i >= 0; i--) {
                verticalLinesArr.push(document.documentElement.offsetWidth - (136+i*270));
            }
            this['verticalLines'] = verticalLinesArr;
        }
    }
    dotParametrs3.horizontalCount();
    dotParametrs3.verticalCount();

    let dot = new Image();
    dot.src = dotParametrs.src;
    let dot2 = new Image();
    dot2.src = dotParametrs2.src;
    let dot3 = new Image();
    dot3.src = dotParametrs2.src;
    function drawDot() {
        ctx.clearRect(0,0,canvas2.width,canvas2.height);
        ctx.drawImage(dot, dotParametrs.x, dotParametrs.y);
        ctx.drawImage(dot2, dotParametrs2.x, dotParametrs2.y);
        ctx.drawImage(dot3, dotParametrs3.x, dotParametrs3.y);
        drawChevrons();
    }
    function drawChevrons() {
        ctx.save();
        ctx.lineWidth = 1;
        ctx.strokeStyle  = 'rgb(255, 255, 255)';
        ctx.beginPath();
        ctx.moveTo(document.documentElement.offsetWidth - (125+2*260), 620);
        ctx.lineTo(document.documentElement.offsetWidth - (125+2*264), 628);
        ctx.stroke();
        ctx.closePath();
        ctx.restore();
        ctx.save();
        ctx.lineWidth = 1;
        ctx.strokeStyle  = 'rgb(255, 255, 255)';
        ctx.beginPath();
        ctx.moveTo(document.documentElement.offsetWidth - (125+2*264), 628);
        ctx.lineTo(document.documentElement.offsetWidth - (125+2*268), 620);
        ctx.closePath();
        ctx.stroke();
        ctx.closePath();
        
        ctx.restore();
        ctx.save();
        ctx.lineWidth = 1;
        ctx.strokeStyle  = 'rgb(255, 255, 255)';
        ctx.beginPath();
        ctx.moveTo(document.documentElement.offsetWidth - (125+2*260), 630);
        ctx.lineTo(document.documentElement.offsetWidth - (125+2*264), 638);
        ctx.stroke();
        ctx.closePath();
        ctx.restore();
        ctx.save();
        ctx.lineWidth = 1;
        ctx.strokeStyle  = 'rgb(255, 255, 255)';
        ctx.beginPath();
        ctx.moveTo(document.documentElement.offsetWidth - (125+2*264), 638);
        ctx.lineTo(document.documentElement.offsetWidth - (125+2*268), 630);
        ctx.closePath();
        ctx.stroke();
        ctx.closePath();
    }
    let direction;
    function verticalMove(parametrs) {
        if (!parametrs.horizontalLines.includes(parametrs.y)) {
            parametrs.y += parametrs.speed;
        }
        else {
            parametrs.x += parametrs.speed;
            if(parametrs.horizontalLines.includes(parametrs.y) && parametrs.verticalLines.includes(parametrs.x)) {
                direction = Math.round(Math.random());
                direction ? (parametrs.y -= parametrs.speed) : (parametrs.y += parametrs.speed)
            }
        }
        if (parametrs.y < 0 || parametrs.y > canvas.height || parametrs.x < 0 || parametrs.x > canvas.width) {
            parametrs.speed = parametrs.speed * -1;
        }
    }

    function loop() {
      verticalMove(dotParametrs);
      verticalMove(dotParametrs2);
      verticalMove(dotParametrs3);
      drawDot();
      window.requestAnimationFrame(loop);
    }

    function init() {
        drawLines();
        window.requestAnimationFrame(loop);
        
    }
    if (!window.matchMedia('(max-width: 576px)').matches) {
        init();
    }
    
}